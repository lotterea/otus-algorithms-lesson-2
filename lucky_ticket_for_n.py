def lucky_ticket(n):
    d = dict()
    for i in range(10 ** n):
        s = 0
        while i > 0:
            s += i % 10
            i = i // 10
        if s in d:
            d[s] += 1
        else:
            d[s] = 1
    return sum([x * x for x in d.values()])


print(lucky_ticket(int(input('n: '))))
